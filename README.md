# 计算机毕业设计-ssm电影售票系统 电影院管理系统 电影购票系统 电影选座系统-周帅组

#### 介绍
开发技术 : springmvc+spring+mybatis(ssm)框架 mysql数据库 支付宝沙箱支付 layUI 百度echarts图表 redis缓存中间件
特色：支付、可视化、智能选座等

#### 软件架构
ssm redis layUI 支付宝沙箱支付 百度echarts


#### 运行截图

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231820_e695cf8d_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231834_d188ec62_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231848_1208c86e_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231856_2c07dcd2_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231905_614ce7f6_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231915_39841b88_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231925_e2771805_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231936_b0f7691c_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231947_5b6c4215_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/231956_18191b2e_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232006_2b26e94a_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232015_9c097863_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232024_78b21f0f_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232034_f26d1b69_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232046_ddd052da_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232056_ea2542ca_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232104_faf301a0_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232112_80ec78b8_7914095.png "计算机毕业设计")

![计算机毕业设计](https://images.gitee.com/uploads/images/2020/0812/232120_2b6395b4_7914095.png "计算机毕业设计")



